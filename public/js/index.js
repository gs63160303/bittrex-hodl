
var canTrade = true;
$(function() {
	//$('#div-api-keys').hide();
	var $wrapper = $('#wrapper');

	// SHOW CURRENT INPUT VALUES 
	$('select.selectized,input.selectized', $wrapper).each(function() {
		var $container = $('<div>').addClass('value').html('Current Value: ');
		var $value = $('<span>').appendTo($container);
		var $input = $(this);
		var update = function(e) { $value.text(JSON.stringify($input.val())); }

		$(this).on('change', update);
		update();

		$container.insertAfter($input);
	});

	// LOAD MARKETS
	// read markets from market.json to JSON variable
	markets = JSON.parse(markets).result
	//console.log(markets);

	// fill dropdown menu with market names
	dropdown_html = '<option value="-1">Select market..</option>';
	for (var i = 0; i < markets.length; i++) {
		dropdown_html += '<option value="' + markets[i].MarketName + '">' + markets[i].MarketCurrency + '-' + markets[i].BaseCurrency + '</option>';
	}
	$('#select-market').html(dropdown_html)

	// ON TITLE PRESS
	$('#h2-title').dblclick(function() {
		$('#div-api-keys').toggle();
	});

	// ON SET API KEYS BUTTON PRESS
	$('#btn-set-keys').click(function() {
		var public = $('#input-api-public').val();
		var secret = $('#input-api-secret').val();
		if (public && secret) {
			socket.emit('set-api', {
				public: public,
				secret: secret
			});
		}
	});

	// ON SUBMIT BUTTON PRESS
	$('#btn-place-trade').click(function() {
		// on submit trade button click send requst to server
		// to place and control trade

		var market = $('#select-market').val();
		var quantity = Number($('#input-quantity').val());
		var	enter = Number($('#input-enter').val());
		var	take1 = Number($('#input-take-lv1').val());
		var	take2 = Number($('#input-take-lv2').val());
		var	take3 = Number($('#input-take-lv3').val());
	    var	stop = Number($('#input-stop').val());

	    if (canTrade == true && market != -1 && quantity > 0 && enter > 0 && take1 > 0 && stop > 0) {
	    	if (take2 > 0) {
	    		if (take2 >= take1) {
					socket.emit('place-trade', {
						market: market,
						quantity: quantity,
						enter: enter,
				    	take1: take1,
				    	take2: take2,
				    	take3: take3,
				    	stop: stop,
				    	status: -1
					});
	    		}
	    	}

			/*$('#select-market').val(-1);
			$('#input-quantity').val('');
			$('#input-enter').val('');
			$('#input-take-lv1').val('');
			$('#input-take-lv2').val('');
			$('#input-take-lv3').val('');
	    	$('#input-stop').val('');*/
	    } else if (canTrade == false) {
	    	$('#div-api-keys').show();
	    	$('#input-api-public').focus();
	    } else if (market == -1) {
	    	$('#select-market').focus();
	    }
	});

	$('#lvl3').hide();

	// ON SOCKET
	var socket = io.connect();
	socket.on('connect', function(data) {
		//socket.emit('join', 'Hello World from client');
	});

	socket.on('set-api-res', function (data) {
		if (data.success == true) {
			canTrade = true;
			if (data.api) {
				$('#input-api-public').val(data.api.public);
				$('#input-api-secret').val('');
			}
			$('#div-api-keys').hide();

			// Update open orders
			updateOpenTrades(data.trades);
			//console.log(data.trades);
		} else {
			$('#div-api-keys').show();
			canTrade = false;
		}
	});

	socket.on('errorMessage', function(data) {
		if (data.message == 'APIKEY_INVALID') {
			canTrade = false;
			$('#div-api-keys').show();
	    	$('#input-api-public').focus();
		}
		console.log(data);
	});

	socket.on('currentPrice', function(data) {
		$('#' + data.id).val(data.price);
	});

	socket.on('new-trade', function(data) {
		updateOpenTrades(data);
		//console.log(data);
	});

	socket.on('status-update', function(data) {
		//console.log(data);
		$('#td-trade-' + data.tradeId + '-status').html(parseStatus(data.status));
		if (data.status == 6) {
			$('#row-trade-'+data.tradeId).remove();
		}
	});

	// ON LABEL PRESS (Set current price)
	$('#prices div label').click(function() {
		if ($('#select-market').val() != -1) {
			socket.emit('currentPrice', {market:  $('#select-market').val(), id: $(this).attr('for')});
		}
	});

	$('#set-quantity').click(function() {
		$('#' + $(this).attr('for')).val(0.001);
	});

	function parseStatus(n) {
		var status = '';
		if (n == 0) {
			status = 'Active';
		} else if (n == 1) {
			status = 'Placed';
		} else if (n == 2) {
			status = 'Checking';
		} else if (n == 3) {
			status = 'Trailing';
		} else if (n == 4) {
			status = 'Sell';
		} else if (n == 5) {
			status = 'Selling';
		} else if (n == 6) {
			status = 'Finished';
		}
		return status;
	}

	function updateOpenTrades(trades) {
		jQuery.each(trades, function(i, trade) {
			var row = "<tr id='row-trade-"+i+"'>\
			<td id='td-trade-"+ i +"-market'>"+trade.market+"</td>\
			<td id='td-trade-"+ i +"-quantity'>"+trade.quantity+"</td>\
			<td id='td-trade-"+ i +"-stop'>"+trade.stop+"</td>\
			<td id='td-trade-"+ i +"-enter'>"+trade.enter+"</td>\
			<td id='td-trade-"+ i +"-take1'>"+trade.take1+"</td>\
			<td id='td-trade-"+ i +"-take2'>"+trade.take2+"</td>\
			<td id='td-trade-"+ i +"-status'>"+parseStatus(trade.status)+"</td>\
			</tr>";

			$('#table-trades > tbody:last-child').append(row);
		});
	}

	// CANCLE TRADE ON DOUBLE-CLICK
	$('#tbody-trades').dblclick(function(event) {
		if (event.target != this) {
			var tradeId = event.target.id.split('-')[2];
			//console.log(tradeId);
			socket.emit('cancelTrade', {
				tradeId: tradeId
			});
			$(event.target).parent().remove();
		}
	});
});
