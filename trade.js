/************** USAGE ***************
* var Trade = require('./trade.js');
* new Trade(this, client, data, api)
*************************************/
// add timestamps in front of log messages
require('console-stamp')(console, '[HH:MM:ss.l]');

var method = Trade.prototype;
//console.log('Trade imported');
var Tickers = require('./tickers.js');
var CHECK_ZONE = 0.07;
//Tickers.start();

var INTERVAL = 5000 // in milliseconds

// data - JSON with info about trade... market, quantity...
function Trade(main, socket, data, api) {
	console.log('[', data.tradeId, ']', 'trade initialized. [', data,']');
	this.canceled = false;
	this.status = data.status; // na zacetku -1
	this.api = api;
	this.tradeId = data.tradeId;
	this.main = main;
	this.data = data;
	this.socket = socket;
	this.bittrex = require('node-bittrex-api');
	/*this.bittrex.options({
		'apikey' : api.public,
		'apisecret' : api.secret
	});*/
    
	//this.checkIfInTrade('gdfgdf');
	//this.checkIfInTrade(this, '0cb4c4e4-bdc7-4e13-8c13-430e587d2cc1', 0.0);
    //console.log(data);

    if (!data.breakeven) {
    	data.breakeven = true;
    }
    data.delta = data.take2 - data.take1;
    data.high = 0;
    data.sell = data.stop;
    data.hit1 = false;
    data.hit2 = false;
    data.l1 = data.take1;
    data.l2 = data.take2;
    this.buying(this, data);
}

method.checkBought = function(self, data) {
	if (self.canceled == false) {
		var status = 2;
		if (self.status != status) {
			self.updateStatus(self, status);
		}

		console.log('[', data.tradeId, ']', 'checkBought @', data.buyPrice, self.api);
		var options = {
			'uuid': data.uuidBuy,
			'apikey' : api.public,
			'apisecret' : api.secret
		};
		self.bittrex.getorder(options, function(response, err) {
			if (err) {
				//self.socket.emit('errorMessage', err);
				Tickers.sendMessage(self.api, 'errorMessage', err);
				return console.log('[', data.tradeId, ']', err);
			} else if (response.success == true) {
				if (response.result.CancelInitiated == false) {
					if (response.result.IsOpen == false) {
						// trade placed
						console.log('[', data.tradeId, '] placed @', response);
						data.quantityAlt = response.result.Quantity;
						self.trailing(self, data);
					} else {
						// check again
						setTimeout(self.checkBought,INTERVAL, self, data);
					}
				} else {
					// order has been canceled
					console.log('[', data.tradeId, '] Order has been canceled by somebody else.');
				}
			} else {
				// some error
				console.log('[', data.tradeId, ']', response);
				//self.socket.emit('errorMessage', response.message);
				Tickers.sendMessage(self.api, 'errorMessage', response.message);
			}
		});		
	}
}

method.buy = function(self, data) {
	if (self.canceled == false) {
		var status = 1;
		if (self.status != status) {
			self.updateStatus(self, status);
		}
		console.log('[', data.tradeId, ']', 'buy ' + data.market + ' @ ' + data.buyPrice);
		var options = {
			'market': data.market,
			'quantity': data.quantity / data.buyPrice,
			'rate': data.buyPrice,
			'apikey' : api.public,
			'apisecret' : api.secret
		};
		
		self.bittrex.buylimit(options, function(response, err) {
			if (err) {
				//self.socket.emit('errorMessage', err);
				Tickers.sendMessage(self.api, 'errorMessage', err);
				return console.log('[', data.tradeId, ']', err);
			}
			else if (response.success == true) {
				data.uuidBuy = response.result.uuid;
				self.checkBought(self, data);
			} else {
				// some error
				console.log('[', data.tradeId, ']', data);
				//self.socket.emit('errorMessage', response.message);
				Tickers.sendMessage(self.api, 'errorMessage', response.message);
			}
		});
	}
}

method.buying = function(self, data) {
	if (self.canceled == false) {
		var status = 0;
		if (self.status != status) {
			self.updateStatus(self, status);
		}
		var tick = self.getTicker(self);
		if (tick <= data.enter) {
			// place order
			// check if bought
			data.buyPrice = data.enter + 0.00000001;
			self.buy(self, data);
		} else {
			// repeat
			setTimeout(self.buying, 1000, self, data);
		}
	}
}

method.checkSold = function (self, data) {
	if (self.canceled == false) {
		var status = 5;
		if (self.status != status) {
			self.updateStatus(self, status);
		}

		var options = {
			'uuid': data.uuidSell,
			'apikey' : api.public,
			'apisecret' : api.secret
		};

		self.bittrex.getorder(options, function(response, err) {
			if (err) {
				//self.socket.emit('errorMessage', err);
				Tickers.sendMessage(self.api, 'errorMessage', err);
				return console.log('[', data.tradeId, ']', err);
			} else if (response.success == true) {
				if (response.result.CancelInitiated == false) {
					if (response.result.IsOpen == false) {
						// trade sold - end program
						console.log('[', data.tradeId, ']', response);
						self.closeTrade(self);
					} else {
						// check again
						setTimeout(self.checkSold, INTERVAL, self, data);
						//self.calcSellPrice(self, uuid, quantity);
					}
				} else {
					// order has been canceled
					console.log('[', data.tradeId, ']', 'Order has been canceled by somebody else.');
				}
			} else {
				// some error
				console.log('[', data.tradeId, ']', response);
				//self.socket.emit('errorMessage', response.message);
				Tickers.sendMessage(self.api, 'errorMessage', response.message);
			}
		});
	}
};

method.sell = function(self, data) {
	if (self.canceled == false) {
		var status = 4;
		if (self.status != status) {
			self.updateStatus(self, status);
		}

		var options = {
			'market': data.market,
			'quantity': data.quantityAlt,
			'rate': data.sellPrice,
			'apikey' : api.public,
			'apisecret' : api.secret
		};
		
		self.bittrex.selllimit(options, function(response, err) {
			if (err) {
				//self.socket.emit('errorMessage', err);
				Tickers.sendMessage(self.api, 'errorMessage', err);
				return console.log('[', data.tradeId, ']', err);
			}
			else if (response.success == true) {
				data.uuidSell = response.result.uuid;
				self.checkSold(self, data);
			} else {
				// some error
				console.log('[', data.tradeId, ']', response);
				//self.socket.emit('errorMessage', response.message);
				Tickers.sendMessage(self.api, 'errorMessage', response.message);
			}
		});
	}
}

method.trailing = function(self, data) {
	if (self.canceled == false) {
		var status = 3;
		if (self.status != status) {
			self.updateStatus(self, status);
		}

		var new_high = false; 
		var END = false;
		var tick = self.getTicker(self);
		if (tick > data.high) {
			data.high = tick;
			new_high = true;
		}
		if (tick <= data.sell) {
			// sell (quantity, tick)
			data.sellPrice = tick - 0.00000001;
			self.sell(self, data);
			console.log('[', data.tradeId, ']', 'sell at: ' + tick);
			END = true;
		}

		if (new_high == true) {
			console.log('[', data.tradeId, ']', 'new high! at: ' + data.high);
			if (data.hit1 == false && tick >= data.l1) {
				data.hit1 = true;
				console.log('[', data.tradeId, ']', 'l1 hit');
				if (data.breakeven == true) {
					console.log('[', data.tradeId, ']', 'sell at breakeven');
					data.sell = data.enter;
				}
			} 
			if (data.hit2 == false && tick >= data.l2) {
				console.log('[', data.tradeId, ']', 'l2 hit');
				data.hit2 = true;
			}

			if (data.hit1 == true) {
				if (data.hit2 == true) {
					// l1 and l2 were hit
					data.sell = data.high - data.delta;
					console.log('[', data.tradeId, ']', 'new sell at: ' + data.sell);
				} else {
					// l1 was hit, l2 not yet
					if (data.breakeven == true) {
						data.sell = Math.min((data.high - data.l1 + data.enter), data.l1);
						console.log('[', data.tradeId, ']', 'new sell at: ' + data.sell);
					} else {
						data.sell = Math.min((data.high - data.l1 + data.stop), data.l1);
						console.log('[', data.tradeId, ']', 'new sell at: ' + data.sell);
					}
				}
			}		
		}
		if (END == false) {
			setTimeout(self.trailing, 1000, self, data);
		}
	}
};

method.getTicker = function (self) {
	return Tickers.getTicker(self.data.market);
};

method.closeTrade = function (self, data) {
	if (self.canceled == false) {
		var status = 6;
		if (self.status != status) {
			self.updateStatus(self, status);
		}
		console.log('[', data.tradeId, ']', 'Trade finished');
		self.removeTrade(self);
		return console.log('[', data.tradeId, ']', data);
	}
};

method.updateStatus = function (self, status) {
	if (self.canceled == false) {
		self.status = status;
		Tickers.updateTradeStatus(self.api, self.tradeId, self.status);
		Tickers.sendMessage(self.api, 'status-update', {
			status: status,
			tradeId: self.tradeId
		});
	}
}

method.removeTrade = function (self) {
	if (self.canceled == false) {
		console.log('[', data.tradeId, ']', 'delete trade');
		Tickers.deleteTrade(self.api, self.tradeId);
	}
}

method.updateClient = function (client) {
	this.socket = client;
}

method.cancelTrade = function () {
	this.canceled = true;
}

module.exports = Trade;
