if (!process.env.PORT) {
  process.env.PORT = 8081;
}

// add timestamps in front of log messages
require('console-stamp')(console, '[HH:MM:ss.l]');

api = {
  'public': '51f922f9ce70494899f85242303f01f4',
  'secret': '138e60156d914de59fdd73b9a4912b91'
};
api2 = {
  'public': '6ae85dc810b84c31a851a3713d9144cb',
  'secret': '5fdbebfd89094cacaa1b136180790944'
};

var tradeId = 1;

var Tickers = require('./tickers.js');
Tickers.start();
var Trade = require('./trade.js');
var trades = {}; // {<tradeId>: Trade}

var path = require('path');
var express = require("express");
var app = express(),
  server  = require("http").createServer(app),
  io = require("socket.io")(server),
  session = require("express-session")({
    secret: "my-secret",
    resave: true,
    saveUninitialized: true,
    cookie: {
      expires: new Date(2147483647000)
    }
  }),
  sharedsession = require("express-socket.io-session");

// Attach session
app.use(session);

// Share session with io sockets
io.use(sharedsession(session, {
    autoSave: true
}));


app.use(express.static(__dirname + '/node_modules'));
app.use('/js', express.static(path.join(__dirname, 'public', 'js')));
app.use('/css', express.static(path.join(__dirname, 'public', 'css')));

app.get('/', function(req, res) {
  // !req.session.api
  res.sendFile(__dirname + '/public/index.html');
  
});

app.get('/test', function(req, res) {
  res.sendFile(__dirname + '/public/test.html');
});

io.sockets.on('connection', function(client) {

  console.log('Client ' + client.handshake.address + ' connected. API:', client.handshake.session.api);
  if (!client.handshake.session.api) {
    client.emit('set-api-res', {
      success: false
    });
  } else {
    Tickers.setClientForApi(client.handshake.session.api, client);
    //console.log(Tickers.getTradesData(client.handshake.session.api));
    /*var t = Tickers.getTrades(client.handshake.session.api);
    t[0].updateClient(client);*/
    client.emit('set-api-res', {
      success: true
      ,api: client.handshake.session.api
      ,trades: Tickers.getTradesData(client.handshake.session.api)
    });
  }

  client.join('currentPrice');

  client.on('join', function(data) {
    console.log('join');
  });

  client.on('place-trade', function(data) {
    console.log('CLIENT:', client.handshake.address, 'PLACED TRADE [', tradeId, ']:', data.market);
    //console.log(client.handshake.session.api.public)
    //console.log(Tickers.trades);
    data['tradeId'] = tradeId;
    Tickers.newTrade(client.handshake.session.api, tradeId, data);
    trades[tradeId] = new Trade(this, client, data, client.handshake.session.api);
    //console.log(Tickers.getTradesData());
    var tmp = {};
    tmp[tradeId] = data
    client.emit('new-trade', tmp);
    tradeId += 1;
  });

  client.on('set-api', function(data) {
    if (testApi(data)) {
      console.log('CLIENT:', client.handshake.address, 'SET API:', data);
      client.handshake.session.api = data;
      client.handshake.session.save();
      Tickers.setClientForApi(client.handshake.session.api, client);
      Tickers.init(data);
      //console.log(client.handshake.session.api);
      //console.log(Tickers.getTradesData(data));
      client.emit('set-api-res', {
        success: true, 
        api: data,
        trades: Tickers.getTradesData(data)
      });
    } else {
      console.log('CLIENT:', client.handshake.address, 'TRIED TO SET AN INVALID API');
    }
  });

  client.on('messages', function(data) {
    client.emit('broad', data);
    client.broadcast.emit('broad', data);
  });

  client.on('currentPrice', function(data) {
    data.price = Tickers.getTicker(data.market);
    client.emit('currentPrice', data);
  });

  client.on('cancelTrade', function(data) {
    trades[data.tradeId].cancelTrade();
    delete trades[data.tradeId];
    Tickers.deleteTrade(client.handshake.session.api, data.tradeId);
  });

});

function testApi(api) {
  /*var bittrex = require('node-bittrex-api')

  bittrex.options({
    'apikey' : api.public,
    'apisecret' : api.secret,
  });

  bittrex.getbalance({ currency : 'BTC' }, function( data, err ) {
    if (err) {
      if (err.message == 'APIKEY_INVALID') {
        console.log('invalid api');
        return false;
      }
    }
    console.log('valid api');
    return true;
  });*/
  return true;
}

// Start the server.
server.listen(process.env.PORT, () => console.log('Listening on http://' + server.address().address + ':' + server.address().port));
