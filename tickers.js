//console.log('Tickers imported');
var bittrex = require('node-bittrex-api');

// add timestamps in front of log messages
require('console-stamp')(console, '[HH:MM:ss.l]');

var ticks = {};
var tradesData = {};	// {<api.public>: {<tradeId>: tradeData}, ...}}
var clients = {};		// {<api.public>: socket}

exports.start = function () {
	//console.log('Tickers start');
	var coins = require('./public/m.json').result;
	var markets = [];
	for (var i = 0; i < coins.length; i++) {
		markets.push(coins[i].MarketName)
	}
	// console.log(markets);

	bittrex.websockets.listen(function(data, client) {
		if (data.M === 'updateSummaryState') {
			//console.log(data.A[0].Deltas.length);
			data.A.forEach(function(data_for) {
				data_for.Deltas.forEach(function(marketsDelta) {
					ticks[marketsDelta.MarketName] = marketsDelta.Last
					//console.log('' +marketsDelta.MarketName, marketsDelta.Last);
				});
			});
			//console.log(Object.keys(ticks).length + '/' + markets.length);
		}
	});
}

exports.getTicker = function (market) {
	return ticks[market];
}

exports.getTradesData = function (api) {
	return tradesData[api.public];
}


exports.newTrade = function (api, tradeId, tradeData) {
	if (!tradesData[api.public]) {
		tradesData[api.public] = {}
	}
	tradesData[api.public][tradeId] = tradeData;
	//console.log(tradesData);
}

exports.deleteTrade = function (api, tradeId) {
	delete tradesData[api.public][tradeId];
}

exports.init = function (api) {
	if (!tradesData[api.public]) {
		tradesData[api.public] = {}
	}
}

exports.updateTradeStatus = function (api, tradeId, status) {
	//trades[api.public][tradeId]['status'] = status;
	tradesData[api.public][tradeId]['status'] = status;
}

exports.setClientForApi = function (api, socket) {
	clients[api.public] = socket;
}

exports.sendMessage = function (api, channel, message) {
	clients[api.public].emit(channel, message);
}